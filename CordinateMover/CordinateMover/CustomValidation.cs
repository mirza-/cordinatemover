﻿using System.Collections.Generic;
using System.Linq;

namespace CordinateMover
{
    public static class CustomValidation
    {
        public static bool IsLenght3(this string[] x)
        {
            return x.Length == 3;
        }
        public static bool IsLenght2(this string[] x)
        {
            return x.Length == 2;
        }
        public static bool IsNumber(this string input)
        {
            int temp;
            return int.TryParse(input, out temp);
        }
        public static bool ValidHeading(this string input)
        {
            return (input == "N" || input == "S" || input == "E" || input == "W");
        }
        public static bool ValidMove(this List<int> movementInput)
        {
            return !(movementInput.Where(n => !(n == 0 || n == 1 || n == 2 || n == 3 || n == 4)).ToArray().Length > 0);
        }
    }
}
