﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CordinateMover
{
    class Room
    {
        public Room() { }
        public Room(int x, int y)
        {
            RoomX = x;
            RoomY = y;
        }
        public int RoomX { get; set; }
        public int RoomY { get; set; }
    }
}
