﻿namespace CordinateMover
{
    class Player
    {
        public Player() { }
        public Player(int x, int y, char heading)
        {
            PlayerX = x;
            PlayerY = y;
            PlayerHeading = heading;
        }
        public int PlayerX { get; set; }
        public int PlayerY { get; set; }
        public char PlayerHeading { get; set; }

        public void PlayerMove(int input)
        {
            if (PlayerHeading.Equals('N'))
            {
                if (input == 1) PlayerY -= 1;
                else if (input == 2) PlayerY += 1;
                else if (input == 3) PlayerHeading = 'E';
                else if (input == 4) PlayerHeading = 'W';
            }
            else if (PlayerHeading.Equals('S'))
            {
                if (input == 1) PlayerY += 1;
                else if (input == 2) PlayerY -= 1;
                else if (input == 3) PlayerHeading = 'W';
                else if (input == 4) PlayerHeading = 'E';
            }
            else if (PlayerHeading.Equals('W'))
            {
                if (input == 1) PlayerX -= 1;
                else if (input == 2) PlayerX += 1;
                else if (input == 3) PlayerHeading = 'N';
                else if (input == 4) PlayerHeading = 'S';
            }
            else if (PlayerHeading.Equals('E'))
            {
                if (input == 1) PlayerX += 1;
                else if (input == 2) PlayerX -= 1;
                else if (input == 3) PlayerHeading = 'S';
                else if (input == 4) PlayerHeading = 'N';
            }
        }
    }
}
