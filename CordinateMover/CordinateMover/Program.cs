﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CordinateMover
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] roomSizeSplit;
            string[] positionSplit;
            List<int> movementInput = null;

            #region Room
            while (true)
            {
                Console.WriteLine("Enter the size of the room:");
                string roomSize = Console.ReadLine();
                roomSizeSplit = roomSize.Split(',', ' ').Where(s => !string.IsNullOrWhiteSpace(s)).ToArray();

                if (roomSizeSplit.IsLenght2() && roomSizeSplit[0].IsNumber() && roomSizeSplit[1].IsNumber())
                    break;
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid data. Try again.");
                    Console.ResetColor();
                }
            }
            Room room = new Room(int.Parse(roomSizeSplit[0]), int.Parse(roomSizeSplit[1])); //Instansiating/Creating the room
            #endregion

            #region Player Start
            while (true)
            {
                Console.WriteLine("Enter your starting position and heading:");
                string position = Console.ReadLine();
                positionSplit = position.Split(',', ' ').Where(s => !string.IsNullOrWhiteSpace(s)).ToArray();
                if (positionSplit.IsLenght3() && positionSplit[0].IsNumber() && positionSplit[1].IsNumber() && positionSplit[2].ValidHeading())
                    break;
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid data. Try again.");
                    Console.ResetColor();
                }
            }

            Player player = new Player(int.Parse(positionSplit[0]), int.Parse(positionSplit[1]), Convert.ToChar(positionSplit[2])); //Instansiating/Creating the player
            #endregion

            #region Voyage
            while (true)
            {
                Console.WriteLine("How would you like to go:");
                string voyage = Console.ReadLine();

                try
                {
                    movementInput = voyage.Split(',', ' ').Where(s => !string.IsNullOrWhiteSpace(s)).Select(int.Parse).ToList();
                }
                catch (Exception)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid data. Try again.");
                    Console.ResetColor();
                }

                if (movementInput != null && movementInput.ValidMove())
                    break;
            }

            foreach (var input in movementInput)
            {
                if (input == 0)
                    break;
                player.PlayerMove(input);
            }
            #endregion

            #region Result
            if ((player.PlayerX > room.RoomX) || (0 > player.PlayerX) || (player.PlayerY > room.RoomY || (0 > player.PlayerY)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Unsuccessful :( the player hit a wall and is out of bound.");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Successful :) Your end position is: [{0},{1}] and your heading is: {2}", player.PlayerX, player.PlayerY, player.PlayerHeading);
                Console.ResetColor();
            }
            Console.ReadLine();
            #endregion          
        }
    }
}
