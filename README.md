# CordinateMover

Hey

I took the liberty of renaming stuff a bit with inspiration from games.
Instead of calling the moving piece "the object", I call it "player"
and instead of "table / matrix" I call it room.

I also changed so that you can input with either comma or space and you can choose which direction you want to start with, instead of having the default value North.
I also divided it so that you first choose how big a room is, and then where you are positioned, instead of having everything at once.
I was also thinking of replacing the input numbers, 1,2,3,4 to F (forward), B (back), R (right), L (left) but thought that I might have gone too far from the original task.

How to run:

First you are asked how big of a room / game board you want, for example:

Enter the size of the room: 4 4

Then you are asked which starting position and direction you want, for example:

Enter your starting position and heading: 2 2 N

Finally, you are asked how you want to go, for example:

 How would you like to go: 1 4 1 3 2 3 2 4 1 0

Successful :) Your end position is: [0,1] and your heading is: N
